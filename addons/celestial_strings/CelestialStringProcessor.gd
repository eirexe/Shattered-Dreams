extends Skeleton2D

class HairPhysicsNode:
	var bone: Bone2D
	var virtual_pos: Vector2
	
var last_pos: Vector2
	
var hair_nodes: Array = []
	
export var GRAVITY := 40.0

export var hair_size_factor := 10.0
export var hair_size_base := 13.0
export var dampening_factor := 0.75
export var iter_count: int = 40

export var turbulence_frequency := 20.0
export var turbulence_amplitude := 4.0

enum MODE {
	RECT,
	CIRC
}

export var mode: int = MODE.CIRC
	
onready var rng = RandomNumberGenerator.new()
	
func add_bone(node):
	if node is CelestialStringNode:
		node.virtual_pos = node.global_position
		hair_nodes.append(node)
	for child in node.get_children():
		if child is CelestialStringNode:
			add_bone(child)
	
func _ready():
	last_pos = global_position
	add_bone(self)

var t = 0.0

func _physics_process(delta):
	t += delta
	var movement = global_position - last_pos
	hair_nodes[0].virtual_pos = hair_nodes[0].global_position
	for i in range(hair_nodes.size()):
		var ro = hair_nodes[i] as CelestialStringNode
		if i == 0:
			continue
		var f = i / float(hair_nodes.size())
		ro.virtual_pos -= movement * 1.0
		var org = ro.to_global(ro.rest.get_origin())
		org = org - ro.global_transform.get_origin()
		ro.virtual_pos.y -= GRAVITY * delta
		ro.virtual_pos = ro.virtual_pos.move_toward(hair_nodes[i-1].virtual_pos + org, (ro.return_home_vel - (ro.return_home_vel * f * dampening_factor)) * delta)
		
		# Turbulence
		var turbulence = turbulence_amplitude * sin(turbulence_frequency * (t + (i / float(hair_nodes.size() - 1) * get_global_transform().get_scale().x)))
		ro.virtual_pos += Vector2(0.0, turbulence) * clamp(movement.length(), 0.0, 1.0)
	for i in range(iter_count):
		apply_constraints(delta)
	for i in range(hair_nodes.size()):
		if i == 0:
			continue
		var node = hair_nodes[i]
		node.global_position = node.virtual_pos
	last_pos = global_position
	update()
func apply_constraints(delta: float):
	for i in range(hair_nodes.size() - 1):

		var parent_node = hair_nodes[i]
		var child_node = hair_nodes[i+1]

		var diff = parent_node.virtual_pos - (child_node.virtual_pos)
		var dist = (parent_node.virtual_pos as Vector2).distance_to(child_node.virtual_pos)
		var len_diff = 0.0
		var max_dist = child_node.rest.get_origin().length()
		if dist > 0 and dist > max_dist:
			len_diff = (max_dist - dist) / dist
		var translation = diff * (0.5 * len_diff)
		if i == 0:
			child_node.virtual_pos -= translation * 2.0
		else:
			parent_node.virtual_pos += translation
			child_node.virtual_pos -= translation
