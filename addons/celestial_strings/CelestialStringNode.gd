tool
extends Bone2D

class_name CelestialStringNode

var virtual_pos := Vector2.ZERO

export var circle_radius: float = 5 setget set_circle_radius
func set_circle_radius(val):
	circle_radius = val
	if Engine.editor_hint:
		update_texture()
export var circle_color = Color.white setget set_circle_color
func set_circle_color(val):
	circle_color = val
	update()

export var return_home_vel: float = 350.0

export var texture: ImageTexture = ImageTexture.new()

func _ready():
	material = preload("res://shaders/jitter_free_scaling_material.tres")
	if texture.get_size().length() == 0:
		update_texture()

func update_texture():
	var circle_diameter := circle_radius * 2.0
	if fmod(circle_diameter, 2.0) != 0.0:
		circle_diameter += 1.0
	var img := Image.new()
	img.create(circle_diameter, circle_diameter, false, Image.FORMAT_RGBA8)
	img.lock()
	var radius := circle_diameter / 2.0
	for x in range(-radius, radius):
		var x_real: float = x + 0.5
		for y in range(-radius, radius):
			var y_real: float = y + 0.5
			if (x_real*x_real+y_real*y_real) <= (radius) * (radius):
				img.set_pixel(x + radius, y + radius, Color.white)
	img.unlock()
	texture.create_from_image(img, 0)
	property_list_changed_notify()
	update()

func _draw():
	draw_texture(texture, -texture.get_size()/2.0, circle_color)
