tool
extends EditorPlugin

func _enter_tree():
	add_custom_type("CelestialStringNode", "Bone2D", preload("res://addons/celestial_strings/CelestialStringNode.gd"), null)
	add_custom_type("CelestialStringProcessor", "Skeleton2D", preload("res://addons/celestial_strings/CelestialStringProcessor.gd"), null)


func _exit_tree():
	remove_custom_type("CelestialStringNode")
	remove_custom_type("CelestialStringProcessor")
