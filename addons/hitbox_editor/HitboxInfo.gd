tool
extends Resource

class_name HitboxInfo

enum HitboxType {
	HURTBOX,
	HITBOX
}

export(HitboxType) var hitbox_type: int
export var shape: Shape2D

func _init(p_hitbox_type: int = HitboxType.HURTBOX):
	hitbox_type = p_hitbox_type

func _to_string():
	return "HitboxInfo"
