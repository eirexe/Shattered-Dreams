tool
extends Resource

class_name AnchorFrameData

export var anchor_name := "new_anchor" setget set_anchor_name
func set_anchor_name(val):
	anchor_name = val
	emit_signal("changed")
export var position: Vector2 setget set_position
func set_position(val):
	position = val
	emit_signal("changed")

var preview_scene: PackedScene setget set_preview_scene

func set_preview_scene(val):
	preview_scene = val
	emit_signal("changed")
