tool
extends Resource

class_name CharacterHitboxData

export var animations: Dictionary

export var sprite_frames: SpriteFrames
