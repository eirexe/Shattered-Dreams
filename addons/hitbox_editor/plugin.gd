tool
extends EditorPlugin

const MainPanel = preload("res://addons/hitbox_editor/HitboxEditor.tscn")

var main_panel_instance

var _sprite_frames_open_file_dialog: FileDialog
var _sprite_frames_save_file_dialog: FileDialog

var _current_sprite_frames_path: String

var current_hitbox_data: CharacterHitboxData

var resources_to_save: Array = []

func _enter_tree() -> void:
	main_panel_instance = MainPanel.instance()
	
	main_panel_instance.connect("open_sprite_frames", self, "_on_open_sprite_frames")
	main_panel_instance.connect("inspect", self, "_inspect_item")
	
	# Add the main panel to the editor's main viewport.
	get_editor_interface().get_editor_viewport().add_child(main_panel_instance)
	# Hide the main panel. Very much required.
	make_visible(false)
	
	_sprite_frames_open_file_dialog = FileDialog.new()
	_sprite_frames_open_file_dialog.connect("file_selected", self, "_on_sprite_frames_opened")
	get_editor_interface().get_base_control().add_child(_sprite_frames_open_file_dialog)
	_sprite_frames_open_file_dialog.mode = FileDialog.MODE_OPEN_FILE
	_sprite_frames_open_file_dialog.filters = ["*.tres ; TRES", "*.res ; RES"]
	
	_sprite_frames_save_file_dialog = FileDialog.new()
	_sprite_frames_save_file_dialog.connect("file_selected", self, "_on_sprite_frames_saved")
	get_editor_interface().get_base_control().add_child(_sprite_frames_save_file_dialog)
	_sprite_frames_save_file_dialog.mode = FileDialog.MODE_SAVE_FILE
	_sprite_frames_save_file_dialog.filters = _sprite_frames_open_file_dialog.filters
	
func _inspect_item(item: Object):
	get_editor_interface().inspect_object(item)

func _exit_tree() -> void:
	if main_panel_instance:
		main_panel_instance.queue_free()
	if _sprite_frames_open_file_dialog:
		_sprite_frames_open_file_dialog.queue_free()
	if _sprite_frames_save_file_dialog:
		_sprite_frames_save_file_dialog.queue_free()

func has_main_screen() -> bool:
	return true

func save_external_data():
	save_current_hitbox_data()

func make_visible(visible) -> void:
	if main_panel_instance:
		main_panel_instance.visible = visible

func get_plugin_name() -> String:
	return "HitboxED"

func _on_open_sprite_frames() -> void:
	_sprite_frames_open_file_dialog.popup_centered(Vector2(1024, 720))
	
func _on_sprite_frames_opened(path: String) -> void:
	_current_sprite_frames_path = path
	_sprite_frames_save_file_dialog.popup_centered(Vector2(1024, 720))

func _on_sprite_frames_saved(save_path: String) -> void:
	var hitbox_data := CharacterHitboxData.new()
	hitbox_data.resource_path = save_path
	hitbox_data.sprite_frames = load(_current_sprite_frames_path) as SpriteFrames
	current_hitbox_data = hitbox_data
	save_current_hitbox_data()
	
func save_current_hitbox_data() -> void:
	if current_hitbox_data:
		ResourceSaver.save(current_hitbox_data.resource_path, current_hitbox_data)

func handles(object: Object) -> bool:
	return object is CharacterHitboxData
	
func edit(obj_to_edit: Object):
	if obj_to_edit is CharacterHitboxData:
		current_hitbox_data = obj_to_edit
		save_current_hitbox_data()
		main_panel_instance.edit_hitbox_data(obj_to_edit)

func get_plugin_icon() ->  Texture:
	return ImageTexture.new()
