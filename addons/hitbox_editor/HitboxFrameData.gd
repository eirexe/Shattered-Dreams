tool
extends Resource

class_name HitboxFrameData

export var hitboxes: Array

export var anchors: Array = []

func _init(p_hitboxes: Array = []):
	hitboxes = p_hitboxes

func _to_string():
	return "HitboxFrameData"
