tool
extends Control

signal open_sprite_frames
signal inspect(object)

onready var animation_list: Tree = get_node("HSplitContainer/Panel3/MarginContainer/ScrollContainer/VBoxContainer2/AnimationList")
onready var animated_sprite: AnimatedSprite = get_node("HSplitContainer/Control/PreviewScene/AnimatedSprite2D")
onready var frame_spinbox: SpinBox = get_node("HSplitContainer/Panel3/MarginContainer/ScrollContainer/VBoxContainer2/HBoxContainer2/Frame")
onready var scene_container: Control = get_node("HSplitContainer/Control")
onready var hitbox_info_list: Tree = get_node("HSplitContainer/Panel3/MarginContainer/ScrollContainer/VBoxContainer2/HitboxInfoList")
onready var preview_scene_root: Node2D = get_node("HSplitContainer/Control/PreviewScene")
onready var anchor_list: Tree = get_node("HSplitContainer/Panel3/MarginContainer/ScrollContainer/VBoxContainer2/AnchorList")

var current_sprite_frames: SpriteFrames setget ,get_current_sprite_frames

func get_current_sprite_frames() -> SpriteFrames:
	return current_hitbox_data.sprite_frames

var current_hitbox_data: CharacterHitboxData
var current_animation: String
var collision_shapes: Array#[CollisionShape2D]
var selected_anchor: AnchorFrameData
var current_anchor_preview_scene_packed: PackedScene
var current_anchor_preview: Node2D
class DebugCollisionShape:
	extends Node2D
	
	var hitbox_info: HitboxInfo
	
	func _draw():
		var col := Color.red
		if hitbox_info.hitbox_type != 0:
			col = Color.blue
		col.a = 0.25
		hitbox_info.shape.draw(get_canvas_item(), col)

var current_frame: int setget set_current_frame, get_current_frame
func set_current_frame(val):
	frame_spinbox.set_block_signals(true)
	frame_spinbox.value = val
	frame_spinbox.set_block_signals(false)

func get_current_frame() -> int:
	return int(frame_spinbox.value)

func _ready():
	scene_container.connect("resized", self, "_update_sprite_position")

func edit_hitbox_data(hitbox_data: CharacterHitboxData):
	current_hitbox_data = hitbox_data
	load_sprite_frames(hitbox_data.sprite_frames)
	select_animation(get_current_sprite_frames().get_animation_names()[0])

func load_sprite_frames(sprite_sheet: SpriteFrames) -> void:
	animation_list.clear()
	animated_sprite.frames = sprite_sheet
	var animation_list_root := animation_list.create_item()
	for animation in sprite_sheet.animations:
		var item = animation_list.create_item(animation_list_root)
		var animation_name: String = animation["name"]
		item.set_text(0, animation_name)
		if not animation_name in current_hitbox_data.animations:
			current_hitbox_data.animations[animation_name] = []
		while (current_hitbox_data.animations[animation_name] as Array).size() <  get_current_sprite_frames().get_frame_count(animation_name):
			current_hitbox_data.animations[animation_name].append(HitboxFrameData.new())
	_update_sprite_position()
func show_frame(frame: int) -> void:
	for shape in collision_shapes:
		shape.queue_free()
	var previously_selected_anchor_list_item := anchor_list.get_selected()
	var anchor_to_select := ""
	if selected_anchor:
		anchor_to_select = selected_anchor.anchor_name
	collision_shapes = []
	current_frame = frame
	animated_sprite.frame = frame
	_disconnect_current_anchor()
	clear_anchor_list()
	clear_hitbox_info_list()
	for shape in (current_hitbox_data.animations[current_animation][frame] as HitboxFrameData).hitboxes:
		add_shape_to_editor(shape)
	for anchor in (current_hitbox_data.animations[current_animation][frame] as HitboxFrameData).anchors:
		var anchor_tree_item := add_anchor_to_editor(anchor)
		prints(anchor.anchor_name, anchor_to_select)
		if anchor.anchor_name == anchor_to_select:
			print("ACHO ALREADY EXISTS??")
			anchor_tree_item.select(0)
func select_animation(animation_name: String) -> void:
	current_animation = animation_name
	animated_sprite.animation = animation_name
	frame_spinbox.max_value = get_current_sprite_frames().get_frame_count(animation_name) - 1
	
	show_frame(0)

func _update_sprite_position() -> void:
	animated_sprite.position = scene_container.rect_size / 2.0

func _on_HSplitContainer_dragged(_offset: float) -> void:
	_update_sprite_position()

func clear_anchor_list() -> void:
	anchor_list.clear()
	anchor_list.create_item()

func clear_hitbox_info_list() -> void:
	hitbox_info_list.clear()
	hitbox_info_list.create_item()

func add_shape_to_editor(hitbox_info: HitboxInfo):
	var root := hitbox_info_list.get_root()
	var new_item := hitbox_info_list.create_item(root)
	var hitbox_type = "HIT"
	var color := Color.red
	if hitbox_info.hitbox_type != 0:
		hitbox_type = "HURT"
		color = Color.blue
	new_item.set_text(0, "Hitbox (%s)" % [hitbox_type])
	new_item.set_meta("hitbox_info", hitbox_info)
	
	var cs := DebugCollisionShape.new()
	cs.hitbox_info = hitbox_info
	collision_shapes.append(cs)
	animated_sprite.add_child(cs)
	
func add_anchor_to_editor(anchor_data: AnchorFrameData) -> TreeItem:
	var root := anchor_list.get_root()
	var new_item := anchor_list.create_item(root)
	new_item.set_text(0, anchor_data.anchor_name)
	new_item.add_button(0, preload("res://addons/hitbox_editor/icons/Remove.svg"))
	new_item.set_meta("anchor_data", anchor_data)
	anchor_data.connect("changed", self, "_update_anchor_name", [anchor_data, new_item])
	return new_item
	
func _update_anchor_name(anchor_data: AnchorFrameData, item) -> void:
	if item:
		item.set_text(0, anchor_data.anchor_name)
	
func _on_AddShape_pressed() -> void:
	var hb := HitboxInfo.new()
	hb.shape = CapsuleShape2D.new()
	current_hitbox_data.animations[current_animation][get_current_frame()].hitboxes.append(hb)
	add_shape_to_editor(hb)

func _on_HitboxInfoList_item_selected() -> void:
	var hitbox_info := hitbox_info_list.get_selected().get_meta("hitbox_info") as HitboxInfo
	emit_signal("inspect", hitbox_info)

func _on_AddAnchorButton_pressed() -> void:
	var anchor := AnchorFrameData.new()
	current_hitbox_data.animations[current_animation][get_current_frame()].anchors.append(anchor)
	add_anchor_to_editor(anchor)

func _on_AnchorList_item_selected():
	var anchor_data = anchor_list.get_selected().get_meta("anchor_data")
	emit_signal("inspect", anchor_data)
	_disconnect_current_anchor()
	selected_anchor = anchor_data
	selected_anchor.connect("changed", self, "_on_anchor_changed")
	_on_anchor_changed()
	
func _disconnect_current_anchor():
	if selected_anchor:
		selected_anchor.disconnect("changed", self, "_on_anchor_changed")
		selected_anchor = null
		if current_anchor_preview:
			current_anchor_preview.queue_free()
		current_anchor_preview = null
		current_anchor_preview_scene_packed = null
		anchor_list.get_selected().deselect(0)
		update()
	
func _on_AnchorList_nothing_selected():
	_disconnect_current_anchor()
	
func _on_anchor_changed():
	if current_anchor_preview_scene_packed != selected_anchor.preview_scene:
		if current_anchor_preview:
			current_anchor_preview.queue_free()
		current_anchor_preview_scene_packed = selected_anchor.preview_scene
		if current_anchor_preview_scene_packed:
			current_anchor_preview = current_anchor_preview_scene_packed.instance()
			current_anchor_preview.show_behind_parent = true
			animated_sprite.add_child(current_anchor_preview)

	update()
	
func _draw():
	if selected_anchor:
		var animated_sprite_global := animated_sprite.to_global(selected_anchor.position)
		var pos := get_global_transform().affine_inverse() * animated_sprite_global
		var col := Color.blue
		draw_circle(pos, 5.0, col)
		
		if current_anchor_preview and current_anchor_preview_scene_packed:
			current_anchor_preview.position = selected_anchor.position
	
func _on_AnchorList_button_pressed(item: TreeItem, _column, _id):
	var anchor_data = item.get_meta("anchor_data")
	current_hitbox_data.animations[current_animation][get_current_frame()].anchors.erase(anchor_data)
	item.free()
	current_hitbox_data.animations[current_animation][get_current_frame()].notify_property_list_changed()
	


