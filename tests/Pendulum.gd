extends Node2D


const PENDULUM_LENGTH = 100.0

var angle = PI / 4

onready var spr = get_node("Sprite")

var angular_velocity = 0.0
var angular_accel = 0.0

var gravity = 60000.0

var lv_pos = Vector2.ZERO

var lv = Vector2.ZERO
var lv_accel = Vector2.ZERO

signal pendulum_pos_changed(pos)

func _ready():
	process_priority = 11

func _physics_process(delta):

	
	angular_accel = (-gravity / PENDULUM_LENGTH) * sin(angle) * delta

	var min_acc = 0
	if abs(angle) < deg2rad(2.0) and abs(angular_velocity) < 2.0:
		min_acc = 1000.0
	if Input.is_action_pressed("move_right"):
		angular_accel += max((min_acc + sin(angle) * -2500.0) * delta, 0.0)
	elif Input.is_action_pressed("move_left"):
		angular_accel += min((-min_acc + sin(angle) * -2500.0) * delta, 0.0)
		
	if Input.is_action_just_pressed("fire_hook"):
		var e = Vector2.DOWN.rotated(-angle - sign(angular_velocity) * deg2rad(90))
		lv = e
		lv_pos = spr.position
		lv_accel = abs(angular_velocity * PENDULUM_LENGTH)
		
	angular_velocity = sign(angular_velocity) * min(abs(angular_velocity), 9.0)

	angular_velocity += angular_accel  * delta

	angular_velocity *= 58 * delta
	angle += angular_velocity * delta
	
	spr.position.x = PENDULUM_LENGTH * sin(angle)
	spr.position.y = PENDULUM_LENGTH * cos(angle)
	
	emit_signal("pendulum_pos_changed", spr)
	
	update()

func _draw():
	draw_line(Vector2.ZERO, spr.position, Color.blue)
	draw_line(lv_pos, lv_pos + lv * lv_accel, Color.red)
