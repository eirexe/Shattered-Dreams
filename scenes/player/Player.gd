extends KinematicBody2D

export var gravity := 600.0

export var ground_move_accelerate := 100.0
export var ground_friction := 1500.0
export var ground_max_speed := 300.0
export var start_ground_move_boost := 100.0

export var air_move_accelerate := 300.0
export var air_friction := 750.0
export var air_max_speed := 300.0

# jump
export var initial_jump_velocity := 1500.0
export var jetpack_acceleration := 3000.0

# Timings, in ms
export var coyote_time_window := 450.0
export var jetpack_max_duration := 100.0
export var jump_buffer_time_window := 100.0

onready var coyote_time_timer := Timer.new()
onready var jetpack_timer := Timer.new()
onready var jump_buffer_timer := Timer.new()

export var camera_offset := Vector2(25.0, 0.0)

export(int, LAYERS_2D_PHYSICS) var hook_collision_layer = 0

# nodes
onready var graphics: Sprite = get_node("Sprite")
onready var camera: Camera2D = get_node("Camera2D")
var was_on_floor := false
var jumps_left = 1
var last_movement = Vector2.ZERO

var linear_velocity := Vector2.ZERO

const CHAIN_PULL = 105

func _ready():
	add_child(jetpack_timer)
	jetpack_timer.wait_time = jetpack_max_duration / 1000.0
	jetpack_timer.one_shot = true
	
	add_child(coyote_time_timer)
	coyote_time_timer.wait_time = coyote_time_window / 1000.0
	coyote_time_timer.one_shot = true
	
	add_child(jump_buffer_timer)
	jump_buffer_timer.wait_time = jump_buffer_time_window / 1000.0
	jump_buffer_timer.one_shot = true
	
	camera.position = camera_offset
	
var hooked = false
var hooked_pos = Vector2.ZERO
var hooked_distance = 0.0
var was_hooked = false
func _physics_process(delta):
	var movement := Vector2.ZERO
	movement.x = Input.get_action_strength("move_right") - Input.get_action_strength("move_left")
	
	var move_right_accel = ground_move_accelerate
	var friction = ground_friction
	var max_speed = ground_max_speed
	
	var just_left_ground = was_on_floor and not is_on_floor()
	var just_landed = not was_on_floor and is_on_floor()
	
	if just_left_ground:
		coyote_time_timer.start()
	
	if just_landed:
		jetpack_timer.stop()
		jumps_left = 1
		was_hooked = false
	
	# jump buffer
	if jumps_left == 0:
		if Input.is_action_just_pressed("jump"):
			jump_buffer_timer.start()
	
	if Input.is_action_just_pressed("fire_hook"):
		var dss := get_world_2d().direct_space_state
		var o = dss.intersect_ray(global_position, global_position + get_local_mouse_position().normalized() * 200.0, [], hook_collision_layer)
		if not o.empty():
			hooked = true
			hooked_pos = o.position
			hooked_distance = to_local(o.position).length()
			print(hooked_distance)
	elif Input.is_action_just_released("fire_hook"):
		hooked = false
			
	
	# Jumping mechanic
	var can_jump = (is_on_floor() or not coyote_time_timer.is_stopped()) and jumps_left > 0
	can_jump = can_jump or (is_on_floor() and not jump_buffer_timer.is_stopped())
	if can_jump:
		var should_jump = Input.is_action_just_pressed("jump") or not jump_buffer_timer.is_stopped()
		if should_jump:
			linear_velocity.y -= initial_jump_velocity
			jetpack_timer.start()
			jump_buffer_timer.stop()
			jumps_left -= 1
	if not jetpack_timer.is_stopped():
		if Input.is_action_just_released("jump"):
			jetpack_timer.stop()
		else:
			linear_velocity.y -= jetpack_acceleration * delta
	
	# Air values
	if not is_on_floor():
		move_right_accel = air_move_accelerate
		friction = air_friction
		max_speed = air_max_speed
	
	if is_on_floor() and movement.x != 0.0 and last_movement.x == 0.0:
		linear_velocity.x = sign(movement.x) * start_ground_move_boost
	
	linear_velocity.x += move_right_accel * movement.x
	
	var gravity_force = gravity
	
	if hooked:
		was_hooked = true
		update()

		friction = 0.0
#		linear_velocity += to_local(hooked_pos)
	elif was_hooked:
		pass
		#friction = 0.0
	
	# Friction integration
	linear_velocity.x = move_toward(linear_velocity.x, 0.0, friction * delta)
	# Speed clamping
	linear_velocity.x = clamp(linear_velocity.x, -max_speed, max_speed)
	linear_velocity.y = clamp(linear_velocity.y, -max_speed, max_speed)
	linear_velocity.y += gravity_force * 2.0 * delta
	was_on_floor = is_on_floor()
	
	linear_velocity = move_and_slide(linear_velocity, Vector2.UP)
	
	if hooked:
		if to_local(hooked_pos).length() > hooked_distance:
			var starting_pos = global_position
			var current_length = to_local(hooked_pos).length()
			current_length = move_toward(current_length, hooked_distance, 1000.0 * delta)
			var target_pos = hooked_pos + hooked_pos.direction_to(global_position) * current_length
			var change = (target_pos - global_position)
			linear_velocity += change / get_physics_process_delta_time()
			linear_velocity = move_and_slide(linear_velocity, Vector2.UP)
			linear_velocity = global_position - starting_pos
	
	# sprite transformation
	if movement.x != 0.0:
		graphics.scale.x = sign(movement.x)
		var cam_off := camera_offset
		cam_off.x *= sign(movement.x)
		camera.position = cam_off
	last_movement = movement
	
func _draw():
	draw_line(Vector2.ZERO, to_local(hooked_pos), Color.white, 1.0)
	draw_line(Vector2.ZERO, linear_velocity * 100.0, Color.red)
