extends Node2D

class_name SDHook

onready var kama = get_node("Sprite2")
onready var hook_graphic = get_node("HookGraphics")
var current_direction: Vector2

const HOOK_IMPULSE_TIME = 100.0

onready var hook_impulse_timer = Timer.new()

var hook_velocity := Vector2.ZERO

const HOOK_GRAVITY := Vector2(0, 1600.0)

enum HookState {
	FIRING,
	HOOKED
}

var state: int = HookState.FIRING

var hooked_position := Vector2.ZERO

signal first_integrated(position)
signal hooked

func _ready():
	set_physics_process(false)
	hook_graphic.set_physics_process(false)
	hook_graphic.connect("physics_integrated", self, "_on_hook_physics_integrated")
	
	add_child(hook_impulse_timer)
	hook_impulse_timer.one_shot = true
	hook_impulse_timer.wait_time = HOOK_IMPULSE_TIME / 1000.0
	
	var first_node = hook_graphic.nodes[0]
	first_node.connect("hit", self, "_on_first_node_hit")
	
func _on_hook_physics_integrated():
	var last_hook_point = hook_graphic.nodes[0]
	var pre_hook_point = hook_graphic.nodes[7]
	var pos_last := last_hook_point.position as Vector2
	var angle = pre_hook_point.position.angle_to_point(pos_last)
	if state == HookState.FIRING:
		kama.rotation = angle - deg2rad(90)
	kama.position = to_local(pos_last)

func _physics_process(delta):
	if state == HookState.HOOKED:
		emit_signal("first_integrated", hook_graphic.nodes[hook_graphic.node_count-1].position)
	hook_graphic.nodes[hook_graphic.node_count-1].position = global_position
	hook_graphic.end = global_position
	
	match state:
		HookState.FIRING:
	
			var start_position = hook_graphic.nodes[0].position
		#	start_position += current_direction * 0.1 * delta
			hook_graphic.nodes[0].position += hook_velocity * delta
			hook_velocity -= hook_velocity * 0.1
		HookState.HOOKED:
			pass
func fire_hook(direction: Vector2):
	hook_velocity = direction * 5050.0
	set_physics_process(true)
	hook_graphic.gravity = Vector2.ZERO
	hook_graphic.teleport_all(global_position)
	current_direction = direction
	hook_impulse_timer.start()
	hook_graphic.set_physics_process(true)
	hook_graphic.show()
func reset_hook():
	set_physics_process(false)
	hook_graphic.set_physics_process(false)
	hook_impulse_timer.stop()
	kama.rotation = 0
	kama.position = Vector2.ZERO
	hook_graphic.nodes[0].pinned = false
	state = HookState.FIRING
	hook_graphic.hide()

func _on_first_node_hit(_result: Physics2DTestMotionResult):
	var hooked_pos = _result.collision_point
	hooked_position = hooked_pos
	hook_graphic.nodes[0].pinned = true
	hook_graphic.nodes[0].position = hooked_pos
	state = HookState.HOOKED
	hook_graphic.gravity = HOOK_GRAVITY
	hook_graphic.hide()
	emit_signal("hooked")
	
