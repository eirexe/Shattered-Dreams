extends Node2D

signal physics_integrated

export(float, 0.0, 1000.0, 1.0) var separation = 2.5
export(int) var node_count = 60
export(Vector2) var gravity := Vector2(0, 1600.0)
export(int) var iterations = 30
export(float) var dampen_factor = 150.0
export(int, LAYERS_2D_PHYSICS) var physics_collision_layer = 0

var nodes := []

class VerletNode:
	signal hit(result)
	var previous_position := Vector2.ZERO
	var position := Vector2.ZERO
	var transform := Transform2D.IDENTITY
	var pinned := false
	
var body: RID
var shape: RID

var end: Vector2
var start: Vector2

onready var line_2d: Line2D = get_node("Line2D")

func _ready():
	body = Physics2DServer.body_create()
	shape = Physics2DServer.circle_shape_create()
	Physics2DServer.shape_set_data(shape, 5.0)
	Physics2DServer.body_add_shape(body, shape)
	Physics2DServer.body_set_space(body, get_world_2d().space)
	Physics2DServer.body_set_mode(body, Physics2DServer.BODY_MODE_KINEMATIC)
	Physics2DServer.body_set_continuous_collision_detection_mode(body, Physics2DServer.CCD_MODE_CAST_SHAPE)
	Physics2DServer.body_set_collision_mask(body, physics_collision_layer)
	for i in range(node_count):
		var node := VerletNode.new()
		nodes.append(node)
		node.position.y -= separation * i
		node.previous_position = node.position
		line_2d.points.append(node.position)
	process_priority = 10
	
	
func _physics_process(delta):
	if Input.is_action_just_released("reel_in"):
		separation = move_toward(separation, 0.1, 0.25)
	if Input.is_action_just_released("reel_out"):
		separation += 0.25
		
	simulate_verlet(delta)
	for i in range(node_count):
		var node = nodes[i] as VerletNode
		move_node(node)
	for _i in range(iterations):
		apply_constraints()
	for i in range(node_count):
		var node = nodes[i] as VerletNode
		move_node(node)
	sync_graphics()
	emit_signal("physics_integrated")
	
func simulate_verlet(delta: float):
	for i in range(node_count):
		var node = nodes[i] as VerletNode
		var t = node.position
		var force = gravity
		
		var new_pos = node.position + (node.position - node.previous_position) * 0.9 + force * (delta * delta)
		
		var motion = new_pos - node.previous_position
#		motion = motion.move_toward(Vector2.ZERO, dampen_factor * motion.length() * (delta * delta))
		node.position = node.previous_position + motion

		node.previous_position = t

func move_node(node: VerletNode) -> void:
	node.transform.origin = node.previous_position
	
	var result = Physics2DTestMotionResult.new()
	var motion = node.previous_position-node.position
#	var qstep_movement = motion / 4.0
#	var total_motion = Vector2.ZERO
#	for i in range(4):
#		node.transform.origin = node.previous_position + total_motion
#		total_motion += qstep_movement
	if Physics2DServer.body_test_motion(body, node.transform, motion, false, 0.01, result):
		node.position += result.motion
		node.emit_signal("hit", result)
	
func apply_constraints():
	for i in range(node_count-1):
		var node_1 := nodes[i] as VerletNode
		var node_2 := nodes[i+1] as VerletNode
		var diff := node_1.position - node_2.position
		var dist := diff.length()
		

		
		var dist_diff := 0.0
		
		if dist > 0:
			dist_diff = (separation - dist) / dist
		
		var translate = diff * (0.5 * dist_diff)
		
		node_1.position += translate
		
		if node_1.pinned:
			node_1.position = node_1.position
			node_2.position -= translate * 2.0
		else:
			node_2.position -= translate
		
func sync_graphics():
	var points = []
	for i in range(node_count):
		var node = nodes[i] as VerletNode
		points.append(to_local(nodes[i].position))
	#points.append(to_local(end))
	line_2d.points = points

func teleport_all(target: Vector2):
	line_2d.points = []
	for i in range(node_count):
		var node = nodes[i] as VerletNode
		node.position = target
		node.previous_position = target
		line_2d.points.append(target)

func apply_force(node: VerletNode, motion: Vector2):
	node.position += motion
